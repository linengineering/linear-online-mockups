<script>

  var linear_speed_force_config = {
      // The type of chart we want to create
      type: 'line',
      // The data for our dataset
      data: {
          datasets: <?php  echo json_encode($linear_speed_vs_force_data_array); ?>
      },

      // Configuration options go here
      options: {
        legend: {
          position: 'right'
        },
        title: {
          display: true,
          text: 'Linear Speed vs. Force',
          fontSize: 16
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            ticks: {
              beginAtZero:true
            },
            scaleLabel: {
                display: true,
                labelString: 'Linear Speed [in/sec]',
                fontSize: 16
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero:true
            },
            scaleLabel: {
                display: true,
                labelString: 'Force [lbs]',
                fontSize: 16
            }
          }]
        }
      }
  };

</script>
