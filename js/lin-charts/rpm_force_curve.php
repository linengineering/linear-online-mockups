<script>

  var rpm_force_config = {
      // The type of chart we want to create
      type: 'line',
      // The data for our dataset
      data: {
          datasets: <?php echo json_encode($rpm_vs_force_chart_data_array); ?>
      },

      // Configuration options go here
      options: {
        legend: {
          position: 'bottom'
        },
        title: {
          display: true,
          text: 'RPM vs. Force',
          fontSize: 16
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            ticks: {
              beginAtZero:true,
              max: 1800
            },
            scaleLabel: {
                display: true,
                labelString: 'Rev/min',
                fontSize: 16
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero:true,
              max: 200
            },
            scaleLabel: {
                display: true,
                labelString: 'Force [lbs]',
                fontSize: 16
            }
          }]
        }
      }
  };

</script>
