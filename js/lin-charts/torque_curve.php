<script>
  var motor_defaults = <?php echo json_encode($motor['defaults']); ?>;
  var voltage = <?php echo json_encode($user_input_voltage); ?>;
  var amperage = <?php echo json_encode($user_input_amperage); ?>;

  var torque_config = {
      // The type of chart we want to create
      type: 'line',
      // The data for our dataset
      data: {
          datasets: [{
              label: motor_defaults['name'] + ', ' + voltage + 'VDC, ' + amperage + ' Amp/Phase, ' + motor_defaults['steppeing'] + ', '+ motor_defaults['winding'],
              backgroundColor: 'rgba(204, 245, 255, 0)',
              borderColor: 'rgb(33, 77, 161)',
              borderWidth: 2,
              pointRadius: 0,
              data: <?php echo json_encode($torque); ?>
          }]
      },

      // Configuration options go here
      options: {
        legend: {
          position: 'top'
        },
        title: {
          display: true,
          text: 'Motor Torque (oz-in)',
          fontSize: 16
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            ticks: {
              beginAtZero:true,
              max: 1800
            },
            scaleLabel: {
                display: true,
                labelString: 'Speed RPM',
                fontSize: 16
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero:true,
              max: 90
            },
            scaleLabel: {
                display: true,
                labelString: 'Torque oz-in',
                fontSize: 16
            }
          }]
        }
      }
  };
</script>
