<script>
  window.onload = function() {
    var ctx_torque_config = document.getElementById('lin-torque-curve').getContext('2d');
    window.torque_config = new Chart(ctx_torque_config, torque_config);

    var ctx_rpm_force_config = document.getElementById('lin-rpm-force').getContext('2d');
    window.torque_config = new Chart(ctx_rpm_force_config, rpm_force_config);

    var ctx_linear_speed_force_config = document.getElementById('lin-linear-speed-force').getContext('2d');
    window.torque_config = new Chart(ctx_linear_speed_force_config, linear_speed_force_config);
  };
</script>
