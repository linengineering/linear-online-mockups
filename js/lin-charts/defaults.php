<script>

  var torque_curve = <?php echo json_encode($N17_C2['torque']); ?>;
  var rpm_vs_force = <?php echo json_encode($WF_a->rpm_vs_force); ?>;
  var linear_speed_vs_force = <?php echo json_encode($WF_a->linear_speed_vs_force); ?>;
  var defaults = <?php echo json_encode($N17_C2['defaults']); ?>;

  //default clors
  var lin_border_color = 'rgb(33, 77, 161)';
  var lin_background_color = 'rgb(204, 245, 255, 0)';

</script>
