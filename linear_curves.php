<?php session_start(); ?>

<?php include_once 'includes/header.php'; ?>

<?php include_once 'includes/data/linear_product_data.php'; ?>
<?php include_once 'includes/classes/LinearCurveCalculator.php'; ?>
<?php include_once 'includes/functions.php'; ?>
<?php include_once 'includes/functions/set-and-generate.php'?>
<style>
p {font-weight: 600;}
</style>
<div class="grid-container">
  <div class="grid-x grid-margin-x">

      <div class="cell medium-3">
        <div class="lin-chart-container">
          <form name="power_input" id="power_input" method="post" action="">
              <p>Your Requirements</p>
              <div class="small-6 cell">
                <label>Force (lbs)</label>
                <input type="text" value='<?php echo $new_load; ?>' name='load' placeholder='Your Load Requirements'/>
              </div>
              <div class="small-6 cell">
                <label>Speed (in/sec)</label>
                <input type="text" value='<?php echo $new_speed; ?>' name='speed' placeholder='Your Speed Requirements'/>
              </div>
              <p style="margin-top:3rem;">Available Power</p>
              <div class="small-6 cell">
                <label>Voltage (V) <i>default 24v</i></label>
                <input type="text" value="<?php echo $new_voltage; ?>" name="voltage"/>
              </div>
              <div class="small-6 cell">
                <label>Amperage (I) <i>default 2amp</i></label>
                <input type="text" value="<?php echo $new_amp; ?>" name="amperage"/>
              </div>
              <input type='submit' class="button" name="update_target" value="UPDATE CHART" />
            </form>
        </div>
      </div>

    <div class="cell medium-9">
      <div class="lin-chart-container">
        <canvas id="lin-linear-speed-force" width="400" height="245"></canvas>
      </div>
    </div>

    <div class="cell medium-6 large-6">
      <div class="lin-chart-container">
        <canvas id="lin-torque-curve" width="400" height="300"></canvas>
      </div>
    </div>

    <div class="cell medium-6 large-6">
      <div class="lin-chart-container">
        <canvas id="lin-rpm-force" width="400" height="300"></canvas>
      </div>
    </div>


    <!-- <div class="button alert large" id='bit-red-button'>BIG RED BUTTON</div>

    <div id="extra-content"></div> -->

  </div>
</div>

<?php include 'js/lin-charts/torque_curve.php'; ?>
<?php include 'js/lin-charts/rpm_force_curve.php'; ?>
<?php include 'js/lin-charts/linear_speed_force_curve.php'; ?>
<?php include 'js/lin-charts/chart_loader.php'; ?>

<script>

$( document ).ready(function() {

  var user_load = $('input[name="load"]').val();
  var user_speed = $('input[name="speed"]').val();

  var lin_bubble_dataset = {
    label: 'Target Speed/Load',
    data: {x:user_speed, y:user_load, r:8},
    type: 'bubble'
  };

  // $.ajaxSetup({
  //   cache: false
  // });
  // var load_url = 'http://linengineering.dev/mockups/linear/includes/functions/ajax_test.php'
  // $('#bit-red-button').on('click', function(){
  //   $('#extra-content').load(load_url);
  // });
  //linear_speed_force_config.data.datasets.unshift(lin_bubble_dataset);

});
</script>

<?php include 'includes/footer.php'; ?>
