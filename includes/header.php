<?php
  function console_log( $data ){
    echo '<script>';
    echo 'console.log('. json_encode( $data ) .')';
    echo '</script>';
  }
?>

<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LinE Mockups</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/lin.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
    <script src="js/vendor/jquery.js"></script>
  </head>
  <body>

    <div class="grid-container">
      <div class="lin-logo-container">
        <img src="includes/images/logo.jpg" />
      </div>
    </div>

    <div class="grid-container">
      <div class="lin-menu-container">
        <ul class="menu">
          <li><a href="#">PRODUCTS</a></li>
          <li><a href="#">ABOUT</a></li>
          <li><a href="#">CONTACT</a></li>
          <li><a href="#">RESOURCES</a></li>
          <li><a href="#">DESIGNER CORNER</a></li>
        </ul>
      </div>
    </div>
