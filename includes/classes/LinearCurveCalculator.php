<?php

class LinearCurveCalculator {

  // Screw variables
  var $torque_curve; // Required input

  var $diameter; // Required input
  var $lead; // Required input
  var $root_diameter; // Required input

  var $pitch_diameter; // Calculated
  var $helix_angle;// Calculated

  // Nut variables
  var $nut_cof = 0.15;  // Calculated
  var $efficiency; // Calculated

  // Calculated arrays
  var $linear_speed = array();
  var $load = array();

  // Cart coordinates
  var $rpm_vs_force = array();
  var $linear_speed_vs_force = array();

  function __construct($torque_curve, $diameter, $lead, $root_diameter){

    //inputs
    $this->torque_curve = $torque_curve;
    $this->diameter = $diameter;
    $this->lead = $lead;
    $this->root_diameter = $root_diameter;

    $this->pitch_diameter();
    $this->helix_angle();
    $this->efficiency();

    $this->linear_speed();
    $this->load();

    $this->rpm_vs_force();
    $this->linear_speed_vs_force();
  }

  function pitch_diameter(){
    $this->pitch_diameter = ($this->diameter + $this->root_diameter) / 2;
  }

  function helix_angle(){
    $this->helix_angle = rad2deg( atan( $this->lead / ( pi() * $this->pitch_diameter ) ) );
  }

  function efficiency(){
    $this->efficiency = (tan(deg2rad($this->helix_angle)) / (tan(deg2rad($this->helix_angle) + atan($this->nut_cof)))) * 100;
  }

  function linear_speed(){
    foreach ($this->torque_curve as $key => $value) {

      $speed_calc = ($value['x'] * $this->lead) / 60;
      array_push($this->linear_speed, $speed_calc);

    }
  }

  function load(){
    foreach ($this->torque_curve as $key => $value) {

      $load_calc = (2 * pi() * $this->efficiency / 100 * $value['y'] / 16) / $this->lead;
      array_push($this->load, $load_calc);

    }
  }

  //charts coordinates
  function rpm_vs_force() {
    $i = 0;
    foreach ($this->torque_curve as $key => $value) {
      $rpm_vs_force_calc = array( 'x' => $value['x'], 'y' => $this->load[$i]);
      array_push($this->rpm_vs_force, $rpm_vs_force_calc);
      $i++;
    }
  }

  function linear_speed_vs_force() {
    $i = 0;
    foreach ($this->linear_speed as $key => $value) {
      $linear_speed_vs_force_calc = array( 'x' => $value, 'y' => $this->load[$i]);
      array_push($this->linear_speed_vs_force, $linear_speed_vs_force_calc);
      $i++;
    }
  }
}

?>
