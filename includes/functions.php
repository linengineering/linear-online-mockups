<?php
function populate_charts($torque, $screws, $new_load, $new_speed){

  $torque = $torque;
  $screws = $screws;
  $array = array(
    'rpm_vs_force' => array(),
    'linear_speed_vs_force' => array()
  );

  //convert string to int
  $new_load = (float) $new_load;
  $new_speed = (float) $new_speed;



  if($new_load != null && $new_speed != null){
    $bubble = array(
      'label' => 'Your Targer',
      'type' => 'bubble',
      'backgroundColor' => '#214da1',
      'borderColor' => '#214da1',
      'data' => array(array('x' => $new_speed, 'y' => $new_load, 'r' => 8))
    );
    array_push($array['linear_speed_vs_force'], $bubble);
    console_log($array['linear_speed_vs_force']);
  };

  foreach($screws as $key => $screw){

    $supplier = $screw['supplier'];
    $pn = $screw['pn'];

    $$pn = new LinearCurveCalculator($torque, $screw['diameter'], $screw['lead'], $screw['root_diameter']);

    $rpm_vs_force_array = array(
      'label' => $pn,
      'backgroundColor' => 'rgba(204, 245, 255, 0)',
      'borderColor' => '#' . mt_rand( 0, 0xFFFFF ),
      'borderWidth' => 2,
      'pointRadius' => 0,
      'data' => $$pn->rpm_vs_force
    );
    array_push($array['rpm_vs_force'], $rpm_vs_force_array);

    $linear_speed_vs_force_array = array(
      'label' => $pn,
      'backgroundColor' => 'rgba(204, 245, 255, 0)',
      'borderColor' => '#' . mt_rand( 0, 0xFFFFF ),
      'borderWidth' => 2,
      'pointRadius' => 0,
      //'borderColor' => 'rgb(204, 245, 255, 1)',
      'data' => $$pn->linear_speed_vs_force
    );
    array_push($array['linear_speed_vs_force'], $linear_speed_vs_force_array);
  }
  return $array;
}
?>
