<?php

$N17_C2 = array(
  'torque' => array(
    array('x'=> 150, 'y'=> 66.24),
    array('x'=> 300, 'y'=> 63.04),
    array('x'=> 600, 'y'=> 48.00),
    array('x'=> 900, 'y'=> 23.40),
    array('x'=> 1200, 'y'=> 10.30),
    array('x'=> 1500, 'y'=> 5.0)
  ),
  'defaults' => array(
    'name' => 'LE17',
    'vdc' => 24,
    'amp_phase' => 2,
    'steppeing' => 'Half Stepping',
    'winding' => 'Bipolar'
  )
);

$screws = array(
  'WF_a' => array(
    'supplier' => 'WF',
    'pn' => 'A',
    'diameter' => 0.2500,
    'lead' => 0.031,
    'root_diameter' => 0.2082
  ),
  // 'WF_b' => array(
  //   'supplier' => 'WF',
  //   'pn' => 'b',
  //   'diameter' => 0.2500,
  //   'lead' => 0.250,
  //   'root_diameter' => 0.1653
  // ),
  // 'PBC_c' => array(
  //   'supplier' => 'PBC',
  //   'pn' => 'c',
  //   'diameter' => 0.2362,
  //   'lead' => 0.039,
  //   'root_diameter' => 0.1969
  // ),
  'PBC_d' => array(
    'supplier' => 'PBC',
    'pn' => 'B',
    'diameter' => 0.2362,
    'lead' => 0.079,
    'root_diameter' => 0.187
  ),
  'PBC_e' => array(
    'supplier' => 'PBC',
    'pn' => 'C',
    'diameter' => 0.2362,
    'lead' => 0.15748,
    'root_diameter' => 0.1959
  ),
  // 'PBC_f' => array(
  //   'supplier' => 'PBC',
  //   'pn' => 'f',
  //   'diameter' => 0.2362,
  //   'lead' => 0.19685,
  //   'root_diameter' => 0.1694
  // ),
  'PBC_g' => array(
    'supplier' => 'PBC',
    'pn' => 'D',
    'diameter' => 0.2362,
    'lead' => 0.23622,
    'root_diameter' => 0.1969
  ),
  // 'PBC_h' => array(
  //   'supplier' => 'PBC',
  //   'pn' => 'h',
  //   'diameter' => 0.2362,
  //   'lead' => 0.31496,
  //   'root_diameter' => 0.1969
  // ),
  // 'PBC_i' => array(
  //   'supplier' => 'PBC',
  //   'pn' => 'i',
  //   'diameter' => 0.2362,
  //   'lead' => 0.3937,
  //   'root_diameter' => 0.1969
  // ),
  'PBC_j' => array(
    'supplier' => 'PBC',
    'pn' => 'E',
    'diameter' => 0.2362,
    'lead' => 0.47244,
    'root_diameter' => 0.1684
  ),
  'PBC_k' => array(
    'supplier' => 'PBC',
    'pn' => 'F',
    'diameter' => 0.2500,
    'lead' => 0.333,
    'root_diameter' => 0.182
  )
);


?>
