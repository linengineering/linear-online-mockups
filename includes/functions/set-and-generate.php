<?php   
$motor = $N17_C2;
$torque = $motor['torque'];

// Get voltage from URL query string if existis. Otherwise, use default.
if(isset($_POST['voltage'])){
  $user_input_voltage = $_POST['voltage'];
} else {
  $user_input_voltage = $motor['defaults']['vdc'];
}

// Get amperage from URL query string if existis. Otherwise, use default.
if(isset($_POST['amperage'])){
  $user_input_amperage = $_POST['amperage'];
} else {
  $user_input_amperage = $motor['defaults']['amp_phase'];
}

if(isset($_POST['load'])){
  $user_input_load = $_POST['load'];
} else {
  $user_input_load = null;
}

if(isset($_POST['speed'])){
  $user_input_speed = $_POST['speed'];
} else {
  $user_input_speed = null;
}

$new_amp = $user_input_amperage;
$new_voltage = $user_input_voltage;
$new_load = $user_input_load;
$new_speed = $user_input_speed;

function voltage_amperage($motor, $new_amp, $new_voltage){

  $default_voltage = $motor['defaults']['vdc'];
  $default_amperage = $motor['defaults']['amp_phase'];

  $new_torque_curve = array();

  foreach ($motor['torque'] as $key => $value) {

    $new_torque = ($new_amp/$default_amperage) * $value['y'];
    $new_speed = ($new_voltage/$default_voltage) * $value['x'];
    $new_torque_point = array('x' => $new_speed, 'y' => $new_torque);
    array_push($new_torque_curve, $new_torque_point);

  }
  return $new_torque_curve;

}

$torque = voltage_amperage($motor, $new_amp, $new_voltage);

$charts_arrays = populate_charts($torque, $screws, $new_load, $new_speed);
$rpm_vs_force_chart_data_array = $charts_arrays['rpm_vs_force'];
$linear_speed_vs_force_data_array = $charts_arrays['linear_speed_vs_force'];
